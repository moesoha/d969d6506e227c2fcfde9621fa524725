#!/bin/sh

set -e

echo "===== Fetching CloudFlare latest IPs ====="
IP4LIST=$(curl https://www.cloudflare.com/ips-v4)
IP6LIST=$(curl https://www.cloudflare.com/ips-v6)

echo "===== Creating Real IP config ====="
printf "$IP4LIST\n\n$IP6LIST\n\n" | sed -E 's/(.+)/set_real_ip_from \1;/' - | tee cloudflare_real_ip.conf
echo "real_ip_header CF-Connecting-IP;" | tee --append cloudflare_real_ip.conf

echo "===== Creating Allow List config ====="
printf "$IP4LIST\n\n$IP6LIST\n\n" | sed -E 's/(.+)/allow \1;/' - | tee cloudflare_allow_list.conf
echo "deny all;" | tee --append cloudflare_allow_list.conf